#!/bin/bash
# Uses systemd to hibernate (may require some privileges, root or polkit wise)
#+if the battery level of _ALL_ batteries (as seen by acpi -b) are below
#+MIN_BATTERY percent, and the power is disconnected (also as seen by acpi -b)

MIN_BATTERY="$1"

batdata=$(acpi -b)
grep -Ew 'Charging|Full' <<< "$batdata" > /dev/null 2>&1 && echo "power on, battery safe" && exit # exit if power is on

should_hibernate=true
max_bat=-1
for line in $(cut -d' ' -f4 <<< "$batdata" | cut -d\% -f1); do
	if [ "$line" -gt "$max_bat" ]; then
		max_bat="$line"
	fi
done

if [ "$max_bat" -lt "$MIN_BATTERY" ]; then
	echo "highest battery level ($max_bat%) below acceptable level ($MIN_BATTERY%)"
else
	echo "highest battery level ($max_bat%) at or above acceptable level ($MIN_BATTERY%)"
	should_hibernate=false
fi

if $should_hibernate; then
	echo "hibernating"
	systemctl hibernate
fi
