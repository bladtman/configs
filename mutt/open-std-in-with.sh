#!/bin/bash
set -euf
cat=/bin/cat
mktemp=/bin/mktemp

suffix="$1"
filename=$($mktemp --suffix=."$suffix")
shift
$cat - > "$filename"
$@ "$filename" &>/dev/null &!
