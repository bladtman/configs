#!/bin/dash -f
# Configure wmii
wmiiscript=wmiirc # For wmii.sh
. wmii.sh


# Configuration Variables
MODKEY=Mod4
ALT=Mod1
UP=k
DOWN=j
LEFT=h
RIGHT=l

# Bars
noticetimeout=2
noticebar=/rbar/!notice

# Colors tuples: "<text> <background> <border>"
export WMII_NORMCOLORS='#d4d5d8 #0d1321 #1d2d44'
export WMII_FOCUSCOLORS='#d4d5d8 #1d2d44 #0d1321'
WMII_BARVALUECOLORS='#d4d5d8 #0d1321 #0d1321'
WMII_BARKEYCOLORS='#b28774 #0d1321 #0d1321'

export WMII_FONT='-*-fixed-medium-r-*-*-13-*-*-*-*-*-*-*'

set -- $(echo $WMII_NORMCOLORS $WMII_FOCUSCOLORS)
export WMII_TERM="urxvt -e tmux"

# Menu history
hist="${WMII_CONFPATH%%:*}/history"
histnum=5000

# Column Rules
wmiir write /colrules <<!
/gimp/ -> 17+83+41
/.*/ -> 50+50 # Golden Ratio
!

# Tagging Rules
wmiir write /tagrules <<!
/vim.*/ -> ~+sel
/.*/ -> sel
!

# Status Bar Info
status60() {
	#Last pacman update, in seconds since the epoch
	lastupdate=$(grep -E "pacman -S[[:alpha:]]*u" /var/log/pacman.log | tail -1 | cut -d" " -f1 | tr -d '[' | xargs -I 'D' date +%s -d "D")
	#Time since last update, in seconds
	secondssinceupdate=$(((`date +%s` - lastupdate)))
	dayssinceupdae=$((secondssinceupdate/60/60/24))"d"

	battery=$(acpi | cut -d" " -f4 | tr -s ",\n" " ")
	charging=$([ "$(acpi --ac-adapter | cut -d' ' -f3)" = "on-line" ] && echo "+" || echo "-")

	keybase_unread=$(keybase chat lsur | grep -E '^\[[0-9]+\][^\*\[]*\*' | wc -l)

	echo -n "$WMII_BARVALUECOLORS\n$keybase_unread"         | wmiir write /rbar/00b-keybasevalue
	echo -n "$WMII_BARVALUECOLORS\n$dayssinceupdae"         | wmiir write /rbar/01b-updatedvalue
	echo -n "$WMII_BARVALUECOLORS\n${battery%?}$charging"   | wmiir write /rbar/02b-batvalue

}
status2() {
	(
		gmails=$(ls -1 ~/.isync_maildir/gmail/inbox/new/ | wc -l)
		echo -n "$WMII_BARVALUECOLORS\n$gmails"        | wmiir write /rbar/07b-gmailsvalue
	)&
	(
		gmailsITU=$(ls -1 ~/.isync_maildir/gmail/_itu/new/ | wc -l)
		echo -n "$WMII_BARVALUECOLORS\n$gmailsITU"     | wmiir write /rbar/08b-gmailsituvalue
	)&

	essid=$(netctl-auto list | grep '^*' | cut -c 3-)
	[ -z $essid ] && essid="none"
	light=$(xbacklight -get | grep -Eo '^[0-9]{1,3}')"%"
	vol=$(amixer get Master | tail -n1 | cut -d" " -f6,8 | tr -d "[]")
	time=$(date "+%a %d %b w%V %R")
	tmuxs=$(tmux ls | wc -l)

	echo -n "$WMII_BARVALUECOLORS\n$essid"                  | wmiir write /rbar/03b-essidvalue
	echo -n "$WMII_BARVALUECOLORS\n$tmuxs"                  | wmiir write /rbar/04b-tmuxsvalue
	echo -n "$WMII_BARVALUECOLORS\n$light"                  | wmiir write /rbar/05b-lightvalue
	echo -n "$WMII_BARVALUECOLORS\n$vol"                    | wmiir write /rbar/06b-volvalue
	echo -n "$WMII_BARVALUECOLORS\n$time"                   | wmiir write /rbar/09b-timevalue
}

local_events() { true;}
wi_runconf -s wmiirc_local

echo $WMII_NORMCOLORS | wmiir create $noticebar

# Event processing
events() {
	cat <<'!'
# Events
Event CreateTag
	echo "$WMII_NORMCOLORS" "$@" | wmiir create "/lbar/$@"
	wmiir xwrite /tag/$@/ctl "colmode 1 default-max"
Event DestroyTag
	wmiir remove "/lbar/$@"
Event FocusTag
	wmiir xwrite "/lbar/$@" "$WMII_FOCUSCOLORS" "$@"
Event UnfocusTag
	wmiir xwrite "/lbar/$@" "$WMII_NORMCOLORS" "$@"
Event UrgentTag
	shift
	wmiir xwrite "/lbar/$@" "*$@"
Event NotUrgentTag
	shift
	wmiir xwrite "/lbar/$@" "$@"
Event LeftBarClick LeftBarDND
	shift
	wmiir xwrite /ctl view "$@"
Event Unresponsive
	{
		client=$1; shift
		msg="The following client is not responding. What would you like to do?$wi_newline"
		resp=$(wihack -transient $client \
			      xmessage -nearmouse -buttons Kill,Wait -print
			      -fn "${WMII_FONT%%,*}" "$msg $(wmiir read /client/sel/label)")
		if [ "$resp" = Kill ]; then
			wmiir xwrite /client/$client/ctl slay &
		fi
	}&
Event Notice
	wmiir xwrite $noticebar $wi_arg

	kill $xpid 2>/dev/null # Let's hope this isn't reused...
	{ sleep $noticetimeout; wmiir xwrite $noticebar ' '; }&
	xpid = $!

# Menus
Menu Client-3-Delete
	wmiir xwrite /client/$1/ctl kill
Menu Client-3-Kill
	wmiir xwrite /client/$1/ctl slay
Menu Client-3-Fullscreen
	wmiir xwrite /client/$1/ctl Fullscreen on
Event ClientMouseDown
	wi_fnmenu Client $2 $1 &

Menu LBar-3-Delete
	tag=$1; clients=$(wmiir read "/tag/$tag/index" | awk '/[^#]/{print $2}')
	for c in $clients; do
		if [ "$tag" = "$(wmiir read /client/$c/tags)" ]; then
			wmiir xwrite /client/$c/ctl kill
		else
			wmiir xwrite /client/$c/tags -$tag
		fi
		if [ "$tag" = "$(wi_seltag)" ]; then
			newtag=$(wi_tags | awk -v't='$tag '
				$1 == t { if(!l) getline l
					  print l
					  exit }
				{ l = $0 }')
			wmiir xwrite /ctl view $newtag
		fi
	done
Event LeftBarMouseDown
	wi_fnmenu LBar "$@" &

# Actions
Action showkeys
	echo "$KeysHelp" | xmessage -file - -fn ${WMII_FONT%%,*}
Action quit
	wmiir xwrite /ctl quit
Action exec
	wmiir xwrite /ctl exec "$@"
Action rehash
	wi_proglist $PATH >$progsfile
Action status
	set +xv
	if wmiir remove /rbar/status 2>/dev/null; then
		sleep 4
	fi

	echo "$WMII_NORMCOLORS" | wmiir create /rbar/status

	wmiir create /rbar/00a-keybaselabel
	wmiir create /rbar/00b-keybasevalue
	wmiir create /rbar/01a-updatedlabel
	wmiir create /rbar/01b-updatedvalue
	wmiir create /rbar/02a-batlabel
	wmiir create /rbar/02b-batvalue
	wmiir create /rbar/03a-essidlabel
	wmiir create /rbar/03b-essidvalue
	wmiir create /rbar/04a-tmuxslabel
	wmiir create /rbar/04b-tmuxsvalue
	wmiir create /rbar/05a-lightlabel
	wmiir create /rbar/05b-lightvalue
	wmiir create /rbar/06a-vollabel
	wmiir create /rbar/06b-volvalue
	wmiir create /rbar/07a-gmailslabel
	wmiir create /rbar/07b-gmailsvalue
	wmiir create /rbar/08a-gmailsitulabel
	wmiir create /rbar/08b-gmailsituvalue
	wmiir create /rbar/09b-timevalue

	echo -n "$WMII_BARKEYCOLORS\nKBUnread:"         | wmiir write /rbar/00a-keybaselabel
	echo -n "$WMII_BARKEYCOLORS\nLastUpdate:"       | wmiir write /rbar/01a-updatedlabel
	echo -n "$WMII_BARKEYCOLORS\nBat:"              | wmiir write /rbar/02a-batlabel
	echo -n "$WMII_BARKEYCOLORS\nEssid:"            | wmiir write /rbar/03a-essidlabel
	echo -n "$WMII_BARKEYCOLORS\nTmux:"             | wmiir write /rbar/04a-tmuxslabel
	echo -n "$WMII_BARKEYCOLORS\nLight:"            | wmiir write /rbar/05a-lightlabel
	echo -n "$WMII_BARKEYCOLORS\nVol:"              | wmiir write /rbar/06a-vollabel
	echo -n "$WMII_BARKEYCOLORS\nInbox:"            | wmiir write /rbar/07a-gmailslabel
	echo -n "$WMII_BARKEYCOLORS\nItu:"              | wmiir write /rbar/08a-gmailsitulabel


	while wmiir read /rbar/status; do
		status2
		sleep 2
	done&

	while wmiir read /rbar/status; do
		status60
		sleep 60
	done

# Key Bindings
KeyGroup Moving around
Key $MODKEY-Right
	currentTag=$(wi_seltag)
	nextTag=$(wi_tags | tr '\n' ' ' | grep -oE "$currentTag.*"  | cut -d' ' -f 2)
	if [ -n "$nextTag" ]; then
		wmiir xwrite /ctl view $nextTag &
	fi
Key $MODKEY-Left
	currentTag=$(wmiir read /tag/sel/ctl | head -n1 | rev)
	nextTag=$(wi_tags | tr '\n' ' ' | rev | grep -oE "$currentTag.*"  | cut -d' ' -f 2 | rev)
	if [ -n "$nextTag" ]; then
		wmiir xwrite /ctl view $nextTag &
	fi
Key $MODKEY-$LEFT   # Select the client to the left
	wmiir xwrite /tag/sel/ctl select left
Key $MODKEY-$RIGHT  # Select the client to the right
	wmiir xwrite /tag/sel/ctl select right
Key $MODKEY-$UP     # Select the client above
	wmiir xwrite /tag/sel/ctl select up
Key $MODKEY-$DOWN   # Select the client below
	wmiir xwrite /tag/sel/ctl select down

Key $MODKEY-space   # Toggle between floating and managed layers
	wmiir xwrite /tag/sel/ctl select toggle

KeyGroup Moving through stacks
Key $MODKEY-Control-$UP    # Select the stack above
	wmiir xwrite /tag/sel/ctl select up stack
Key $MODKEY-Control-$DOWN  # Select the stack below
	wmiir xwrite /tag/sel/ctl select down stack

KeyGroup Moving clients around
Key $MODKEY-Shift-$LEFT   # Move selected client to the left
	wmiir xwrite /tag/sel/ctl send sel left
Key $MODKEY-Shift-$RIGHT  # Move selected client to the right
	wmiir xwrite /tag/sel/ctl send sel right
Key $MODKEY-Shift-$UP     # Move selected client up
	wmiir xwrite /tag/sel/ctl send sel up
Key $MODKEY-Shift-$DOWN   # Move selected client down
	wmiir xwrite /tag/sel/ctl send sel down

Key $MODKEY-Shift-space   # Toggle selected client between floating and managed layers
	wmiir xwrite /tag/sel/ctl send sel toggle

KeyGroup Client actions
Key $MODKEY-f # Toggle selected client's fullsceen state
	wmiir xwrite /client/sel/ctl Fullscreen toggle
Key $MODKEY-Shift-c # Close client
	wmiir xwrite /client/sel/ctl kill

KeyGroup Changing column modes
Key $MODKEY-d # Set column to default mode
	wmiir xwrite /tag/sel/ctl colmode sel default-max
Key $MODKEY-s # Set column to stack mode
	wmiir xwrite /tag/sel/ctl colmode sel stack-max
Key $MODKEY-m # Set column to max mode
	wmiir xwrite /tag/sel/ctl colmode sel stack+max

KeyGroup Changing column sizes
Key $MODKEY-$ALT-l # increase sel frame's size to the right
	wmiir xwrite /tag/sel/ctl grow sel sel right

Key $MODKEY-$ALT-h # increase sel frame's size to the left
	wmiir xwrite /tag/sel/ctl grow sel sel left

Key $MODKEY-$ALT-j # increase sel frame's size downwards
	wmiir xwrite /tag/sel/ctl grow sel sel down

Key $MODKEY-$ALT-k # increase sel frame's size upwards
	wmiir xwrite /tag/sel/ctl grow sel sel up

Key $MODKEY-control-h # decrease sel frame's size from the right
	wmiir xwrite /tag/sel/ctl grow sel sel right -1

Key $MODKEY-control-l # decrease sel frame's size from the left
	wmiir xwrite /tag/sel/ctl grow sel sel left -1

Key shift-control-j # decrease sel frame's from the top
	wmiir xwrite /tag/sel/ctl grow sel sel up -1

Key $MODKEY-shift-control-i # decrease sel frame's from the bottom
	wmiir xwrite /tag/sel/ctl grow sel sel down -1

KeyGroup Running programs
Key XF86AudioMute
	amixer set Speaker unmute
	amixer set Headphone unmute
	amixer set Master toggle
Key XF86AudioRaiseVolume
	amixer set Master 5+
Key XF86AudioLowerVolume
	amixer set Master 5-
Key XF86MonBrightnessDown
	xbacklight -dec 5 -steps 1
Key XF86MonBrightnessUp
	xbacklight -inc 5 -steps 1
Key $ALT-t           # Open terminal
	eval wmiir setsid $WMII_TERM &
Key $ALT-p           # Paste a password
	passFileNames=$(cd /home/bladt/.password-store; find -iname '*.gpg' -type f | cut -d/ -f 2-)
	passNames=$(echo "$passFileNames" | rev | cut -d\. -f 2- | rev)
	userPick=$(echo -n "$passNames" | wimenu)
	[ -n "$userPick" ] && pass "$userPick" | head -n1 |tr -d '\n' | xdotool type --clearmodifiers --file -
Key $MODKEY-a      # Open wmii actions menu
	action $(wi_actions | wimenu -h "${hist}.actions" -n $histnum) &
Key $MODKEY-p      # Open program menu
	eval wmiir setsid "$(wimenu -h "${hist}.progs" -n $histnum <$progsfile)" &
Key $MODKEY-control-shift-h
	eval systemctl hibernate &

Key $MODKEY-control-shift-s
	eval systemctl suspend &

Key $MODKEY-F1
	synclient | grep -E 'TouchpadOff.*= 1' >/dev/null 2>&1
	eval synclient TouchpadOff=$?

KeyGroup Other
Key $MODKEY-Control-t # Toggle all other key bindings
	case $(wmiir read /keys | wc -l | tr -d ' \t\n') in
	0|1)
		echo -n "$Keys" | wmiir write /keys
		wmiir xwrite /ctl grabmod $MODKEY;;
	*)
		wmiir xwrite /keys $MODKEY-Control-t
		wmiir xwrite /ctl grabmod Mod3;;
	esac

KeyGroup Tag actions
Key $MODKEY-t       # Change to another tag
	(tag=$(wi_tags | wimenu -h "${hist}.tags" -n 50) && wmiir xwrite /ctl view $tag) &
Key $MODKEY-Shift-t # Retag the selected client
	c=$(wi_selclient)
	(tag=$(wi_tags | wimenu -h "${hist}.tags" -n 50) && wmiir xwrite /client/$c/tags $tag) &
!
	for i in 0 1 2 3 4 5 6 7 8 9; do
		cat <<!
Key $MODKEY-$i		 # Move to the numbered view
	wmiir xwrite /ctl view "$i"
Key $MODKEY-Shift-$i     # Retag selected client with the numbered tag
	wmiir xwrite /client/sel/tags "$i"
!
	done
}
wi_events events local_events

# WM Configuration
wmiir write /ctl <<!
	font $WMII_FONT
	focuscolors $WMII_FOCUSCOLORS
	normcolors $WMII_NORMCOLORS
	grabmod $MODKEY
	border 1
!

#set new columns layout to default-mode
wmiir xwrite ctl colmode default
#set selected tags layout to default-mode
wmiir xwrite tag/sel/ctl colmode 1 default-max

# Misc
progsfile="$(wmiir namespace)/.proglist"
action status &
wi_proglist $PATH >$progsfile &

# Setup Tag Bar
IFS="$wi_newline"
wmiir rm $(wmiir ls /lbar | sed 's,^,/lbar/,') >/dev/null
seltag=$(wmiir read /tag/sel/ctl | sed 1q)
unset IFS
wi_tags | while read tag
do
	if [ "$tag" = "$seltag" ]; then
		echo "$WMII_FOCUSCOLORS" "$tag"
	else
		echo "$WMII_NORMCOLORS" "$tag"
	fi | wmiir create "/lbar/$tag"
done

wi_eventloop

