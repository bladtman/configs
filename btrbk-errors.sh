#!/bin/bash
#-e abort at error.
#-u fail if unset vars are referenced
set -eu

log=`journalctl -u btrbk-run.service -S $(date -d "2 days ago" -I)`
errors=`grep '!!!' <<< "$log" | grep -v "Target" | sed -r 's/.* !!! //' | sort | uniq`

[ "$errors" = "" ] && errorCount=0 || errorCount=`wc -l <<< "$errors"`

unhandledErrors=0

if [ "$errorCount" -ne 0 ]; then

	correctedCount=`grep -Ff <(echo "$errors") <<< "$log" | grep '>>>|***' | wc -l`
	unhandledErrors=$((errorCount - correctedCount))
fi

echo "$unhandledErrors"
exit "$unhandledErrors"
