[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx 2>&1 > /home/bladt/.xinit_log

# The following lines were added by compinstall
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' ignore-parents parent pwd
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}' 'r:|[._-/\]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 3
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.*' insert-sections true
zstyle ':completion:*:man:*' menu yes select
zstyle :compinstall filename '/home/bladt/.zshrc'

autoload -Uz compinit
autoload -U colors && colors
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=2000
SAVEHIST=10000
# End of lines configured by zsh-newuser-install
setopt correct
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt INC_APPEND_HISTORY
stty -ixon #disables the C-s C-q control flow

function precmd () {
	git=$(git symbolic-ref HEAD 2>/dev/null) #|| RPROMPT="lars" && return
	git="${git#refs/heads/}"
	git="%{$fg[green]%}$git%{$reset_color%}"
	RPROMPT=$git" [%3~]"
}

PROMPT="[%n][%?]%(!.#.$) "
typeset -U path
export GOPATH=/home/bladt/gocode
path+=$GOPATH/bin
export EDITOR=vim
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#gistit access to my github user
export GISTIT_TOKEN="b63808207d0db8d3ee98c3cb3153897b187b82f0"

bindkey -v #vi-mode
bindkey "^[[5~"		history-beginning-search-backward
bindkey "^[[6~"		history-beginning-search-forward
bindkey "^?"		backward-delete-char
bindkey "^[[Z"		reverse-menu-complete

#aliases
alias sudo='sudo ' #space means check aliases in next word
alias mv="mv -i"
alias ls='ls --color=auto -F'
alias ll='ls -hl'
alias la='ls -A'
alias lsd="ls --group-directories-first"
alias lsda="lsd -A"
alias df='df -hT'
alias cp="cp --reflink=auto"
alias free='free -h'
alias l='less'
alias -g g='grep --color=auto -n'
alias -g gi='g -i'
alias -g ge='g -E'
alias -g gv='g -v'
alias nload='nload devices wlan0'
alias cal='cal -m'
alias t="true"
alias info="info --vi-keys"
alias Re="R --slave -e"
alias R="R -q"
alias vim="vim -p" #open files in tabs
alias vimr="vim -R" #readonly
alias git-yoda='git commit -am "$(yoda)"'
alias curl='curl -w "\\nStatus: %{http_code}\\n"'
itu="/home/bladt/documents/itu/msc/2.sem"
itur="/home/bladt/repos/itu/msc/3.sem"
conf="/home/bladt/repos/configs"
man () {
	man=$(/usr/bin/man "$@")
	code=$?
	[[ $code -eq 0 ]] && vim -RM -c "set ft=man nonu nornu nolist nospell breakindent linebreak wrap" -c "noremap // /\v^\W+" <(col -b <<< $man)
	return $code

}
mutt() {
	muttcmd="/usr/bin/mutt"
	pgrep -xa mutt | grep -v -e "-R" && $muttcmd -R || $muttcmd
}
