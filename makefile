.DEFAULT_GOAL := no-default
.PHONY: mirrorlist  /etc/pacman.d/mirrorlist
SHELL:=/bin/bash #for some reason /bin/sh doesn't behave like bash in makefiles?

user: /home/bladt/.tmux.conf /home/bladt/.vimrc /home/bladt/.ssh/config\
	/home/bladt/.Xmodmap /home/bladt/.xinitrc /home/bladt/.zshrc\
	/home/bladt/.Xresources /home/bladt/.wmii/wmiirc /home/bladt/.muttrc\
	/home/bladt/.mbsyncrc\
	/home/bladt/mutt/mailcap

root: /etc/zsh/zshrc /usr/local/bin/wallrotor /etc/ssh/ssh_config\
	/etc/ssh/sshd_config /usr/local/bin/yoda\
	/usr/local/bin/walltester /etc/vimrc /etc/X11/xorg.conf.d/10-evdev.conf\
	/etc/modprobe.d/blacklist.conf\
	/etc/modprobe.d/alsa-base.conf\
	/etc/locale.conf /etc/vconsole.conf\
	/usr/share/kbd/keymaps/i386/qwerty/dk-latin1-esc-caps-reversed.map /boot/loader/loader.conf\
	/boot/loader/entries/arch.conf /etc/sysctl.d/99-sysctl.conf\
	/etc/X11/xorg.conf.d/20-intel.conf /etc/locale.gen\
	/etc/mkinitcpio.conf /etc/X11/xorg.conf.d/50-synaptics.conf\
	/etc/modules-load.d/virtualbox.conf\
	/usr/local/bin/batterysafe /etc/systemd/system/batterysafe.service\
	/etc/systemd/system/batterysafe.timer\
	/etc/netctl/Kalifatet\
	/etc/netctl/BLAP /etc/netctl/eduroam\
	/etc/systemd/system/mbsync.service /etc/systemd/system/mbsync.timer\
	/etc/btrbk.conf /etc/systemd/system/btrbk-run.service\
	/etc/systemd/system/btrbk-hourly.timer /usr/local/bin/btrbk-errors

applists:
	pacmatic -Qqen | sed '$$d' | sort > apps.txt
	pacmatic -Qqem | sed '$$d' | sort > apps_forreign.txt

appcomm:
	@echo "apps.txt"
	@echo -e "installed\tfile:"
	@comm -3 <(pacmatic -Qqen | sed '$$d' | sort) apps.txt
	@echo -e "\napps_forreign.txt"
	@echo -e "installed\tfile:"
	@comm -3 <(pacmatic -Qqem | sed '$$d' | sort) apps_forreign.txt

#goals for "user" files

mirrorlist: /etc/pacman.d/mirrorlist

/etc/pacman.d/mirrorlist:
	reflector -p https -a 2 --sort score -n6 -f3 --save $@

/home/bladt/.mbsyncrc: mbsyncrc /home/bladt/.isync_maildir/
	cp -u -P $< $@

/home/bladt/.wmii/wmiirc: wmiirc
	cp -u -P $<  $@

/home/bladt/.tmux.conf: tmux.conf
	cp -u -P $< $@

/home/bladt/.vimrc: vimrc
	cp -u -P $< $@

/home/bladt/.tmp/vim/undo:
	mkdir -p /home/bladt/.tmp/vim/undo

/home/bladt/.tmp/vim/swp:
	mkdir -p /home/bladt/.tmp/vim/swp

/home/bladt/.ssh/config: ssh_config
	cp -u -P $< $@

/home/bladt/.Xmodmap: Xmodmap
	cp -u -P $< $@

/home/bladt/.xinitrc: xinitrc
	cp -u -P $< $@

/home/bladt/.zshrc: zshrc
	cp -u -P $< $@

/home/bladt/.Xresources: Xresources
	cp -u -P $< $@

/home/bladt/mutt:
	mkdir -p /home/bladt/mutt

/home/bladt/.muttrc: mutt/muttrc
	cp -u -P $< $@

/home/bladt/.Maildir:
	mkdir $@

/home/bladt/mutt/mailcap: /home/bladt/mutt mutt/mutt_mailcap
	cp mutt/mutt_mailcap $@

#goals for "root" files
/etc/systemd/system/btrbk-run.service: systemd-units/btrbk-run.service
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/systemd/system/btrbk-hourly.timer: systemd-units/btrbk-hourly.timer
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/systemd/system/btrbk.conf: btrbk.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/systemd/system/mbsync.service: systemd-units/mbsync.service
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/systemd/system/mbsync.timer: systemd-units/mbsync.timer
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/netctl/eduroam: netctl/eduroam
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/netctl/Kalifatet: netctl/Kalifatet
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/netctl/BLAP: netctl/BLAP
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/systemd/system/batterysafe.timer: systemd-units/batterysafe.timer
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/systemd/system/batterysafe.service: systemd-units/batterysafe.service
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/modules-load.d/virtualbox.conf: modules-load.d/virtualbox.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/X11/xorg.conf.d/50-synaptics.conf: xorg/50-synaptics.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/X11/xorg.conf.d/20-intel.conf: xorg/20-intel.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/sysctl.d/99-sysctl.conf: 99-sysctl.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/boot/loader/entries/arch.conf: gummiboot/arch.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/boot/loader/loader.conf: gummiboot/loader.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/vconsole.conf: vconsole.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/usr/share/kbd/keymaps/i386/qwerty/dk-latin1-esc-caps-reversed.map: dk-latin1-esc-caps-reversed.map
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/locale.gen: locale/locale.gen
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/locale.conf: locale/locale.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/mkinitcpio.conf: mkinitcpio.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/modprobe.d/blacklist.conf: blacklist.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/modprobe.d/alsa-base.conf: alsa-base.conf
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/vimrc: vimrc_root
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/usr/local/bin/yoda: yoda
	rsync --perms --chmod=u=rwx --chmod=g=rx --chmod=o=rx $< $@

/etc/X11/xorg.conf.d/10-evdev.conf: xorg/10-evdev.conf
	rsync --perms --chmod=u=rwx --chmod=g=rx --chmod=o=rx $< $@

/usr/local/bin/batterysafe: batterysafe.sh
	rsync --perms --chmod=u=rwx --chmod=g=rx --chmod=o=rx $< $@

/etc/zsh/zshrc: zshrc_root
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/usr/local/bin/btrbk-errors: btrbk-errors.sh
	rsync --perms --chmod=u=rwx --chmod=g=rx --chmod=o=rx $< $@

/usr/local/bin/wallrotor: wallrotor
	rsync --perms --chmod=u=rwx --chmod=g=rx --chmod=o=rx $< $@

/usr/local/bin/walltester: walltester
	rsync --perms --chmod=u=rwx --chmod=g=rx --chmod=o=rx $< $@

/etc/ssh/ssh_config: ssh_config_root
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

/etc/ssh/sshd_config: sshd_config_root
	rsync --perms --chmod=u=rw --chmod=g=r --chmod=o=r $< $@

#dummy default
no-default:
	$(info use 'make user' or 'make root')
